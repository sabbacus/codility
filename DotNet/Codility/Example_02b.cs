﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/**
 * A non-empty array A consisting of N integers is given. The array contains an odd number of elements, and each element of the array can be paired with another element that has the same value, except for one element that is left unpaired.

For example, in array A such that:

  A[0] = 9  A[1] = 3  A[2] = 9
  A[3] = 3  A[4] = 9  A[5] = 7
  A[6] = 9
the elements at indexes 0 and 2 have value 9,
the elements at indexes 1 and 3 have value 3,
the elements at indexes 4 and 6 have value 9,
the element at index 5 has value 7 and is unpaired.
Write a function:

class Solution { public int solution(int[] A); }

that, given an array A consisting of N integers fulfilling the above conditions, returns the value of the unpaired element.

For example, given array A such that:

  A[0] = 9  A[1] = 3  A[2] = 9
  A[3] = 3  A[4] = 9  A[5] = 7
  A[6] = 9
the function should return 7, as explained in the example above.

Write an efficient algorithm for the following assumptions:

N is an odd integer within the range [1..1,000,000];
each element of array A is an integer within the range [1..1,000,000,000];
all but one of the values in A occur an even number of times.
    */

namespace Codility
{
    public class Solution
    {
        public int solution(int[] ArrayOfNumbers)
        {
            if (ArrayOfNumbers.Length == 1)
            {
                return ArrayOfNumbers[0];
            }

            // int unique;
            var foundOnce = new List<int>();
            var foundTwiceOrMore = new List<int>();

            int lowerIndex = Convert.ToInt32(ArrayOfNumbers.GetLowerBound(0));
            int upperIndex = Convert.ToInt32(ArrayOfNumbers.GetUpperBound(0));

            for (int i = lowerIndex; i <= upperIndex; i++)
            {
                int value = Convert.ToInt32(ArrayOfNumbers.GetValue(i));

                if (foundTwiceOrMore.Exists(x => x == value))
                {

                    continue;

                }
                else if (foundOnce.Exists(x => x == value))
                {
                    foundTwiceOrMore.Add(value);
                    foundOnce.Remove(value);
                    continue;

                }
                else if (!foundOnce.Exists(x => x == value))
                {
                    foundOnce.Add(value);
                    continue;
                }
            }

            // Ok, so you are here. Assume unique.
            Array found = foundOnce.ToArray();

            if (found.Length == 1)
            {
                return Convert.ToInt32(found.GetValue(0));
            }

            return -1;
        }
    }
}
